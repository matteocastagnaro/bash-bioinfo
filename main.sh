#!/bin/bash

info="\n # VR360261\n # Matteo Castagnaro\n # 17 Maggio 2014"

function menu(){
	echo -e "\n\t1) Eliminazione processi\n\t2) Archiviazione file\n\t3) Concatenazione file\n\t4) Esci\n";
}

function eliminazioneproc(){
	_file=".tmp";
	> $_file;
	ctmp=0;
	if test "$1" = "--help";then
		echo -e $info;
		echo -en "
 comando: \033[1m1\033[0m
							
 Con il comando \033[1m1\033[0m si esegue l'uccisione di tutti i processi associati al programma <nomeprog>.\n";
	else
		echo -en "\nNome del programma di cui eliminare i processi: ";
		read np;
		if test -z "$np";then
			echo "Inserisci il nome del programma.";
		else
			#ps ax | grep "$np" | awk '{print $1}' | xargs -i kill {};
    		echo -en "`ps ax | grep "$np"`" > $_file;
			echo -en "\n" >> $_file;
			pid_file=`cat "$_file" | cut -d" " -f2`;
			pid_grep=`cat "$_file" |grep "grep" |cut -d" " -f2`;
			for proc in $pid_file;do
      			if test "$proc" != "$pid_grep";then
					kill -9 "$proc" >> /dev/null;
					ctmp=1;
        		fi
			done
			rm $_file;	
			if test $ctmp = 1;then
				echo "Uccisione di tutti i processi associati al programma '$np'";
			else
				echo "Nessun processo associato al programma '$np'. Nessun kill eseguito";
			fi
		fi
	fi
}

function archiviazionefile(){
	if test "$1" = "--help";then
		echo -e $info;
		echo -en "
 comando: \033[1m2\033[0m
							
 Con il comando \033[1m2\033[0m si comprime una cartella passata dall'utente e si crea un file LOG e uno dove vengono scritti gli errori incontrati durante l'esecuzione.\n";
	else
		echo -en "\nNome della directory da archiviare: ";
		read _dir;
		echo -en "Nome del file di archivio: ";
		read f_arch;
		echo -en "Nome del file .log: ";
		read f_log;
		echo -en "Nome del file .err: ";
		read f_err;echo;
		> $f_log.log;
		> $f_err.err;
		if test -d "$_dir";then
			if test -e "$f_arch.tgz";then
				echo "errore: file $f_arch.tgz già esistente. Scegliere un altro nome.";
				echo "> written something in $f_err.err" >> $f_log.log;
				echo "> error: file $f_arch.tgz already exists in current directory" >> $f_err.err;
			else
				tar cvfz $f_arch.tgz $_dir 2> $f_err.err;
				echo -e "\nArchiviata la cartella $_dir nel file compresso $f_arch.tgz.";
				echo "> Compressed directory $_dir in the folder $f_arch.tgz" >> $f_log.log;
			fi
		else
			echo "errore: directory '$_dir' non esistente.";
			echo "> written something in $f_err.err." >> $f_log.log;
			echo "> error: directory '$_dir' not exist" >> $f_err.err;
		fi
	fi
}

function concatenazionefile(){
	if test "$1" = "--help";then
		echo -e $info;
		echo -en "
 comando: \033[1m3\033[0m
							
 Con il comando \033[1m3\033[0m si concatena una lista di file in un unico file.\n";
	else
		echo -en "Nome del file sul quale concatenare i file: ";
		read f_concat;
		if test -z "$f_concat";then
			echo "Il nome del file non può essere nullo!";
		else
			> $f_concat.txt;
			echo -en "Elenco file da concatenare (separati da uno spazio): ";
			read f_tocat;
			for file in $f_tocat;do
				echo "*** $file ***" >> $f_concat.txt;
				echo "`cat $file`" >> $f_concat.txt;
				echo -e "" >> $f_concat.txt;
			done
		fi
	fi
}

function _exit(){
	if test "$1" = "--help";then
		echo -e $info;
		echo -en "
 comando: \033[1m4\033[0m
							
 Con il comando \033[1m4\033[0m si esce dal programma.\n";
	else
		exit;
	fi
}

while true;do

	menu;
	echo -en "> ";
	read x _help;
	if test "$_help" != "--help" && test "$_help" != "";then
		echo "parametro non riconosciuto digitare: miocomando --help per aiuto";
	else
		case $x in
		1) eliminazioneproc $_help;;
		2) archiviazionefile $_help;;
		3) concatenazionefile $_help;;
		4) _exit $_help;;
		*) echo "$x: command not found";;
		esac	
	fi
done

# VR360261	
# Matteo Castagnaro
# 17 Maggio 2014
# Elaborato 1

